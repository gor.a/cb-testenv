# Clientbase Testing Env

Набор различных вариантов стека над одним дистрибутивом и базой

web access on port 89

## structure

```shell
distr/         # link/folder with clientbase code
db/            # link/folder for mysql data
dbfiles/       # link/folder for /files in db-container
```

## run

Populate initial data for db container
1. Put DATAFILE.sql to ./dbfiles/
2. Run ``docker exec -it DB-CONTAINER mysql -u test -p``
3. Execute in mysql console  ``ALTER DATABASE test COLLATE utf8_general_ci; USE test;``
3. Execute in mysql console  ``source /files/DATAFILE.sql``

